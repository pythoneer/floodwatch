import requests
from datetime import datetime, timedelta
from django.http import Http404
from django.shortcuts import render

API_BASE = "http://environment.data.gov.uk/flood-monitoring/"

def index(request):
    url = API_BASE + "id/3dayforecast"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        issued = data["items"]["issueDatetime"]
        dt = datetime.strptime(issued, "%Y-%m-%dT%H:%M:%S")
        days = [ dt, dt + timedelta(days=1), dt + timedelta(days=2) ]
        context = {
          "summary": data["items"]["forecastSummary"],
          "label": data["items"]["label"],
          "days": days,
        }
        return render(request, "forecast/index.html", context)
    else:
        raise Http404("API call failed")
