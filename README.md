# README

This Django project is a very small first step in an exploration of how
data from the Environment Agency's real-time flood monitoring API can be
integrated with other datasets, such as measurements from rain gauges.

You'll need Python 3, [Django 1.9](http://www.djangoproject.com) and
[Pandas](http://pandas.pydata.org) to run it.  You'll also need a
[Mapbox](https://www.mapbox.com) access token.  Before running, you'll need
to create a `keys.json` file in the top-level directory, containing values
for `SECRET_KEY` and `MAPBOX_ACCESS_TOKEN`.

The programs `addgauge.py` and `addrain.py` in `tools` can be used to populate
the database with rain gauge details and rainfall measurements.  After using
`addgauge.py` to input gauge details, `addrain.py` can be run on the CSV
files of rainfall data from [Leeds Data Mill](http://www.leedsdatamill.org).

In its current form, the site consists of two Django apps that are not
linked together in any way, so you'll need to type in URLs to access the
different features:

* http://localhost:8000/admin to browse rainfall data
* http://localhost:8000/forecast to view a 3-day flood forecast
* http://localhost:8000/rain to view a map of rain gauges

Clicking on the marker for a gauge pops up a link to a page that plots the
rainfall at that gauge, using [plotly.js](https://plot.ly/javascript/).
Currently, there is no date filtering, but you can interact with the plot
to do this manually.
