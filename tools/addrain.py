# Adds rainfall data to the database

import django
django.setup()

import csv, sys
import geo
from datetime import datetime
from django.db import transaction
from rain.models import Gauge, Increment

try:
    filename = sys.argv[1]
except:
    sys.exit("Usage: python rainfall.py <csvfile>")

gauges = Gauge.objects.all()
cache = {}

with transaction.atomic():
    with open(filename, "rt") as csvfile:
        for record in csv.DictReader(csvfile):
            timestamp = datetime.strptime(record["DateTime"], "%d/%m/%Y %H:%M")
            amount = float(record["mm"])

            key = record["Easting"] + record["Northing"]
            if key not in cache:
                easting = float(record["Easting"])
                northing = float(record["Northing"])
                lat, lon = geo.natgrid_to_latlon(easting, northing)
                for gauge in gauges:
                    if gauge.latitude - lat < 0.0001 and gauge.longitude - lon < 0.0001:
                        cache[key] = gauge
                        break

            gauge = cache.get(key)
            increment, added = Increment.objects.get_or_create(gauge=gauge,
              timestamp=timestamp, amount=amount)
            if added:
                print(increment, "added")
