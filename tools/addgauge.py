# Adds a new rain gauge to the database

import django
django.setup()

import geo
from rain.models import Gauge

addr = input("Enter address: ")
easting = float(input("Enter easting: "))
northing = float(input("Enter northing: "))

lat, lon = geo.natgrid_to_latlon(easting, northing)

gauge, added = Gauge.objects.get_or_create(latitude=lat, longitude=lon, address=addr)
if added:
    print("Added:", gauge)
