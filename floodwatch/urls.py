from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r"^forecast/", include("forecast.urls")),
    url(r"^rain/", include("rain.urls")),
]
