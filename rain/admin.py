from django.contrib import admin
from .models import Gauge, Increment

class IncrementAdmin(admin.ModelAdmin):
    date_hierarchy = "timestamp"
    list_display = [ "gauge", "timestamp" ]
    list_filter = [ "gauge" ]
    list_per_page = 50

admin.site.register(Gauge)
admin.site.register(Increment, IncrementAdmin)
