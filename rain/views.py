import pandas as pd
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from .models import Gauge, Increment


def index(request):
    context = {
      "gauges": Gauge.objects.all(),
      "token": settings.MAPBOX_ACCESS_TOKEN
    }
    return render(request, "rain/index.html", context)


def detail(request, id):
    gauge = get_object_or_404(Gauge, pk=id)
    increments = Increment.objects.filter(gauge=gauge)
    timestamps = increments.values_list("timestamp", flat=True)
    rain = increments.values_list("amount", flat=True)

    data = pd.Series(rain, timestamps)
    resampled = data.resample("1H").sum().fillna(0)

    context = {
      "gauge": gauge,
      "time": [ str(t) for t in resampled.index ],
      "rainfall": resampled.values.tolist()
    }
    return render(request, "rain/detail.html", context)
