from django.conf.urls import url
from . import views

app_name = "rain"

urlpatterns = [
  url(r"^$", views.index, name="index"),
  url(r"^(\d+)/$", views.detail, name="detail"),
]
