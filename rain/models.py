from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

min_lat = MinValueValidator(-90.0)
max_lat = MaxValueValidator(90.0)
min_lon = MinValueValidator(-180.0)
max_lon = MaxValueValidator(180.0) 


class Gauge(models.Model):
    """
    Represents a rain gauge.
    """
    latitude = models.FloatField(validators=[min_lat, max_lat])
    longitude = models.FloatField(validators=[min_lon, max_lon])
    address = models.CharField(max_length=150)

    def __str__(self):
        return self.address


class Increment(models.Model):
    """
    Represents an increment in recorded rainfall for a gauge.
    """
    gauge = models.ForeignKey(Gauge)
    timestamp = models.DateTimeField()
    amount = models.FloatField()

    class Meta:
        ordering = [ "timestamp" ]

    def __str__(self):
        return "{} [{:.1f}]".format(
          self.timestamp.strftime("%Y-%m-%d:%H:%M"), self.amount)
